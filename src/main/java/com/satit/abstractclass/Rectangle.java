/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractclass;

/**
 *
 * @author Satit Wapeetao
 */
public class Rectangle extends Shape{
    private final double w;
    private final double h;
    public Rectangle(double w, double h){
        super("Circle");
        this.w=w;
        this.h=h;
    }
    @Override
    public double CalArea() {
        return w*h;
    }
    @Override
    public String toString() {
        return "Rectagle{" + "Area = " + CalArea() + '}';
    }
    
}
