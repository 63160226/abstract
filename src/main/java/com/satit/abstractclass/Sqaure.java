/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractclass;

/**
 *
 * @author Satit Wapeetao
 */
public class Sqaure extends Shape{
    private final double s;   
    public Sqaure(double s){
        super("Sqaure");
        this.s=s;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public double CalArea() {
         return s*s;
    }
     @Override
    public String toString() {
        return "Sqaure{" + "Area = " + CalArea() + '}';
    }
    
}
