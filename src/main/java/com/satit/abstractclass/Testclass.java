/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractclass;

/**
 *
 * @author Satit Wapeetao
 */
public class Testclass {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1.5);
        System.out.println(circle1);
        Circle circle2 = new Circle(4.5);
        System.out.println(circle2);
        Circle circle3 = new Circle(5.2);
        System.out.println(circle3);
        Rectangle rectangle1 = new Rectangle(3,2);
        System.out.println(rectangle1);
        Rectangle rectangle2 = new Rectangle(4,2);
        System.out.println(rectangle2);
        Sqaure sqaure1= new Sqaure(4.0);
        System.out.println(sqaure1);
        Sqaure sqaure2= new Sqaure(2.0);
        System.out.println(sqaure2);
    }
}
